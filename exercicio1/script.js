let input = document.getElementById("input");
let botao = document.getElementById("bto");
let lista = document.getElementById("lista");

botao.addEventListener('click', (e) => {
  if(input.value === "") return;
  let novoElemento = document.createElement("p");
  novoElemento.className = "linha";
  novoElemento.append(input.value);
  lista.prepend(novoElemento);
  e.preventDefault();
});

input.addEventListener('keyup', (e) => {
  let key = e.which || e.keyCode;
  if(key == 13) {
    if(input.value === "") return;
    let novoElemento = document.createElement("p");
    novoElemento.className = "linha";
    novoElemento.append(input.value);
    lista.prepend(novoElemento);
  };
});

lista.addEventListener('click', (e) => {
  lista.removeChild(e.path[0]);
});


